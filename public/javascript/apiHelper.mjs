export const fetchText = async (textId) => {
    const result = await fetch(`/game/texts/${textId}`)
        .then(result => result.json());

    return result
};