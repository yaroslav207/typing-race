import { handleRoomsLogic } from "./components/roomsPage/index.mjs";
import { handleGamesLogic } from "./components/gamePage/index.mjs";
import {addClass, removeClass} from "./helper.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}


const socket = io("", { query: { username } });

const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');

handleRoomsLogic(io, socket, roomsPage);
handleGamesLogic(io, socket, gamePage);

const toggleRoomsPage = () => {
  removeClass(roomsPage, 'display-none');
  addClass(gamePage, 'display-none')
};
const toggleGamePage = () => {
  removeClass(gamePage, 'display-none');
  addClass(roomsPage, 'display-none')
};

const error = (message) => {
  window.alert(message)
};

const errorLogin = (message) => {
  window.alert(message);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

socket.on('JOIN_ROOM_DONE', toggleGamePage);
socket.on('LEAVE_ROOM_DONE', toggleRoomsPage);
socket.on("ERROR_LOGIN", errorLogin);


socket.on('ERROR', error);


