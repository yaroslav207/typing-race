import {createElement} from "../../helper.mjs";

export const createRoomBlock = (roomInfo, joinRoom) => {
    const roomBlock = createElement({
        tagName: 'div',
        className: 'room',
        attributes: {
            id: roomInfo.roomId
        }
    });

    const joinButton = createJoinButton(joinRoom);

    roomBlock.innerHTML = (`
    <span class="count-user">${roomInfo.users.length} users connected</span>
    <span class="title-room">${roomInfo.roomId}</span>
  `);
    roomBlock.appendChild(joinButton);

    return roomBlock;
};

const createJoinButton = (onClick) => {
    const joinButton = createElement({
        tagName: 'button',
        className: 'join-btn',
    });

    joinButton.addEventListener('click', onClick);
    joinButton.innerText = 'JOIN';

    return joinButton
};
