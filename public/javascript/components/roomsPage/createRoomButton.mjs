import {createElement} from "../../helper.mjs";

export const createButtonCreateRoom = (createRoom) => {
    const createRoomButton = createElement({
        tagName: 'button',
        className: 'button-create-room',
        attributes: {
            id: 'add-room-btn'
        }
    });

    createRoomButton.addEventListener('click', createRoom);
    createRoomButton.innerText = 'Create room';

    return createRoomButton;

};
