import { createRoomList, fillRoomList } from "./roomsList.mjs";
import { createButtonCreateRoom } from "./createRoomButton.mjs";

export const handleRoomsLogic = (io, socket, roomsPage) => {
    const createRoom = () => {
        const titleRoom = window.prompt('Enter title room');
        socket.emit('CREATE_ROOM', titleRoom);
    };

    const joinRoom = (id) => () => {
        socket.emit("JOIN_ROOM", id);
    };

    const updateRooms = (rooms) => {
        fillRoomList(rooms, joinRoom);
    };

    const fillRoomPage = () => {
        roomsPage.innerHTML = '<h1>Join room or create new</h1>';
        roomsPage.appendChild(createButtonCreateRoom(createRoom));
        roomsPage.appendChild(createRoomList());
    };
    fillRoomPage();

    socket.on("UPDATE_ROOMS", updateRooms);
};



