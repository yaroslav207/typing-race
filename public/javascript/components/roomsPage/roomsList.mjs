import {createElement} from "../../helper.mjs";
import {createRoomBlock} from "./roomBlock.mjs"

export const createRoomList = () => {
    const roomsList = createElement({
        tagName: 'div',
        className: 'list-rooms',
    });

    return roomsList;
};

export const fillRoomList = (rooms, joinRoom) => {
    const roomsListBlock = document.querySelector('.list-rooms');
    if (!roomsListBlock){
        return
    }
    roomsListBlock.innerHTML = '';

    rooms.forEach( roomInfo => {
        roomsListBlock.appendChild(createRoomBlock(roomInfo, joinRoom(roomInfo.roomId)))
    });
};