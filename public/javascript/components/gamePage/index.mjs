import { createUsersList, updateUsersList } from "./usersList.mjs";
import { createHeaderGamePage } from "./headerPage.mjs";
import {
    changeReadyButton,
    changeTimer,
    addTextToGameBlock,
    createTextBlockForGame,
    startTimer, createGameBlock,
    checkKeysForGame,
    showRatingList,
    changeTimerForGame} from "./gameBlock.mjs";
import { fetchText } from "../../apiHelper.mjs";
import {updateProgressBar} from "./userBlock.mjs";

export const handleGamesLogic = (io, socket, gamesPage) => {

    let textBlock;
    let text;

    const changeStatusUser = () => {
        socket.emit("CHANGE_USER_STATUS");
    };

    const startLoadingGames = async ({idText, secondBeforeStartGame}) => {
        startTimer(secondBeforeStartGame);
        text = await fetchText(idText);
        textBlock = createTextBlockForGame(text);
    };

    const startGame = (secondGame) => {
        addTextToGameBlock(textBlock, secondGame);
        checkKeysForGame(socket, text)
    };

    const changeUserStatus = (status) => {
        changeReadyButton(status)
    };

    const updateRoom = (roomInfo) => {
        updateUsersList(roomInfo.users)
    };

    const leaveRoom = () => {
      socket.emit("LEAVE_ROOM")
    };

    const fillGamePage = (roomInfo) => {
        gamesPage.innerHTML = "";
        gamesPage.appendChild(createHeaderGamePage(roomInfo.roomId, leaveRoom));
        gamesPage.appendChild(createGameBlock(changeStatusUser));
        gamesPage.appendChild(createUsersList(roomInfo));
    };

    const changeProgress = ({userId, progress})=>{
        updateProgressBar(userId, progress)
    };

    socket.on("CHANGE_TIMER_FOR_GAME", changeTimerForGame)
    socket.on("FINISH_GAME", showRatingList);
    socket.on("CHANGE_PROGRESS_USER", changeProgress);
    socket.on("START_GAME", startGame);
    socket.on("CHANGE_TIMER", changeTimer);
    socket.on("START_TIMER", startLoadingGames);
    socket.on("CHANGE_USER_STATUS", changeUserStatus);
    socket.on("UPDATE_USERS_IN_ROOM", updateRoom);
    socket.on("JOIN_ROOM_DONE", fillGamePage);
};