import {createElement} from "../../helper.mjs";

export const createHeaderGamePage = (roomId, leaveRoom) => {
    const headerPage = createElement({
        tagName: 'div',
        className: 'header-game-page'
    });

    headerPage.innerHTML = `<h2>${roomId}</h2>`;
    headerPage.appendChild(createButtonBack(leaveRoom));

    return headerPage;
};

const createButtonBack = (leaveRoom) => {
    const buttonBack = createElement({
        tagName: 'button',
        className: 'btn-back',
    });

    buttonBack.innerText = '< Back to room';
    buttonBack.addEventListener('click', leaveRoom);

    return buttonBack;
};