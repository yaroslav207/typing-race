import {createElement} from "../../helper.mjs";

export const createGameBlock = (changeStatus) => {
    const gameBlock = createElement({
        tagName: 'div',
        className: 'game-block',
        attributes: {
            id: 'game-block'
        }
    });

    gameBlock.appendChild(createReadyButton(changeStatus));

    return gameBlock
};

const createReadyButton = (changeStatus) => {
    const readyButton = createElement({
        tagName: 'button',
        attributes: {
            id: 'ready-btn'
        }
    });

    readyButton.addEventListener('click', changeStatus);
    readyButton.innerText = 'Ready';

    return readyButton;
};

export const changeReadyButton = (status) => {
    const readyButton = document.getElementById('ready-btn');
    if (!readyButton) {
        return
    }

    if (status) {
        readyButton.innerText = 'NOT READY'
    } else {
        readyButton.innerText = 'READY'
    }
};

export const startTimer = (startValue) => {
    const gameBlock = document.getElementById('game-block');
    const timer = createTimer(startValue);

    gameBlock.innerHTML = '';
    gameBlock.appendChild(timer);
};

const createTimer = (startValue) => {
    const timer = createElement({
        tagName: "div",
        className: "timer"
    });
    timer.innerText = startValue;
    return timer;
};

export const changeTimer = (value) => {
    const timer = document.querySelector('.timer');

    timer.innerText = value
};

const createTimerForGame = (startValue) => {
    const timer = createElement({
        tagName: "div",
        className: "timer-for-game"
    });
    timer.innerText = startValue;
    return timer;
};

export const changeTimerForGame = (value) => {
    const timer = document.querySelector('.timer-for-game');

    timer.innerText = value
};

export const addTextToGameBlock = (textBlock, startValue) => {
    const gameBlock = document.getElementById('game-block');

    gameBlock.innerHTML = '';
    gameBlock.appendChild(textBlock);
    gameBlock.appendChild(createTimerForGame(startValue))
};

export const createTextBlockForGame = (text) => {
    const textBlockForGame = createElement({
        tagName: 'div',
        className: 'text-block',
        attributes:{
            id: 'text-container'
        }
    })
    const letterList = text.split('');
    letterList.forEach((letter, index) => {
        textBlockForGame.appendChild(createLetter(letter, index))
    });

    return textBlockForGame
};

const createLetter = (letter, index) => {
        const letterBlock = createElement({
            tagName: 'span',
            className: 'letter',
            attributes:{
                id: `letter${index}`
            }
        });
        letterBlock.innerText = letter;
        return letterBlock;
};

const changeStyleLetter = (index, sizeText) => {
    if(index >= 0 && index < sizeText){
        const letter = document.getElementById(`letter${index}`);
        letter.style.textDecoration = 'underline'
    }
    if((index - 1) >= 0 && (index - 1) < sizeText){
        const letter = document.getElementById(`letter${index - 1}`);
        letter.style.backgroundColor = 'green';
        letter.style.textDecoration = 'none';
    }
};

export const checkKeysForGame = (socket, text) => {
    const listLetter = text.split('');
    const body = document.querySelector('body');
    let i = 0;
    changeStyleLetter(i, listLetter.length);
    const keyCheck = (event) => {
        if(i >= listLetter.length){
            body.removeEventListener('keydown', keyCheck);
            return
        }
        if (event.key === listLetter[i]) {
            socket.emit("CHANGE_PROGRESS");
            i++;
            changeStyleLetter(i, listLetter.length);
        }
    };

    body.addEventListener('keydown', keyCheck);
};

export const showRatingList = (list) => {
    const gameBlock = document.getElementById('game-block');
    const ratingList = createRatingList(list);

    gameBlock.innerHTML = "";
    gameBlock.appendChild(ratingList);
};

const createRatingList = (list) => {
    const ratingList = createElement({
        tagName: 'div',
        className: 'rating-list',
    })

    list.forEach((user, index) => {
        ratingList.appendChild(createRatingItem(user, index + 1))
    })

    return ratingList
};

const createRatingItem = (user, result) => {
    const listItem = createElement({
        tagName: 'div',
        className: 'rating-list-item',
    })

    listItem.innerHTML = `<span>${result}. </span><span id="place-${result}">${user.name}</span>`;
    return listItem
};




