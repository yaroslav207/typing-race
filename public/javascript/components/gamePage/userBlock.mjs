import {createElement} from "../../helper.mjs";

export const createUserBlock = (userInfo) => {
    const roomBlock = createElement({
        tagName: 'div',
        className: 'user-block',
        attributes: {
            id: userInfo.name
        }
    });


    roomBlock.innerHTML = (`
    <div class="status-user ${userInfo.status?'ready-status-green':'ready-status-red'}" ></div>
    <span class="name-user">${userInfo.name}</span>
    <div class="progress-bar"><div style="width: ${userInfo.percentageProgress}%" class="progress user-progress ${userInfo.name}" ></div></div>
  `);

    return roomBlock;
};

export const updateProgressBar = (userId, progress) => {
    const progressBar = document.querySelector(`#${userId} .progress`);
    console.log(userId)
    if(!progressBar){
        return
    }

    progressBar.style.width = progress + '%';
    if(progress === 100){
        progressBar.style.backgroundColor = '#00FF00';
    }
};



