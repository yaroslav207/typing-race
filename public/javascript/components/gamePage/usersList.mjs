import {createElement} from "../../helper.mjs";
import {createUserBlock} from "./userBlock.mjs";

export const createUsersList = (roomInfo) => {
    const roomsList = createElement({
        tagName: 'div',
        className: 'list-users',
    });
    console.log(roomInfo);
    roomInfo.users.forEach((userInfo) => {
        roomsList.appendChild(createUserBlock(userInfo))
    });

    return roomsList;
};

export const updateUsersList = (users) => {
    const roomsListBlock = document.querySelector('.list-users');
    if (!roomsListBlock){
        return
    }
    roomsListBlock.innerHTML = '';

    users.forEach( userInfo => {
        roomsListBlock.appendChild(createUserBlock(userInfo))
    });
};