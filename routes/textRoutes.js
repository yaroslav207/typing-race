import { Router } from "express";
import {texts} from "../data";

const router = Router();

router
    .get("/:id", (req, res) => {
        const text = texts[req.params.id];
        res.json(text);
    });

export default router;