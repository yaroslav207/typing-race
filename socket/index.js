import * as config from "./config";
import {texts} from "../data";

let usersList = [];

const checkUser = (username) => {
    return usersList.includes(username)
};
const addUserList = (username) => {
    usersList.push(username);
};
const removeUserList = (username) => {
    usersList = usersList.filter(user => user !== username);
};

let rooms = [];

const createUser = (username) => ({
    name: username,
    status: false,
    progress: 0,
    percentageProgress: 0
});
const joinUserInRoom = (roomId, username) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.users = [...room.users, createUser(username)];
        }
        return room;
    });
};
const findRoom = (roomId) => {
    return rooms.find(room => roomId === room.roomId)
};
const findStatusUser = (roomId, username) => {
    return findUserInfoInRoom(roomId, username).status
};
const findUserInfoInRoom = (roomId, username) => {
    return findRoom(roomId).users.find(user => user.name === username)
};
const deleteRoom = (roomId) => {
    rooms = rooms.filter(room => roomId !== room.roomId)
};
const deleteUserInRoom = (roomId, username) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.users = room.users.filter(user => user.name !== username)
        }
        return room
    })
};
const changeUserStatus = (roomId, username) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.users = room.users.map(user => {
                if (user.name === username) {
                    user.status = !user.status
                }
                return user
            })
        }
        return room
    })
};
const changeUserProgress = (roomId, username) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.users = room.users.map(user => {
                if (user.name === username) {
                    user.progress += 1;
                    user.percentageProgress = Math.floor((user.progress / room.target) * 100)
                }
                return user
            })
        }
        return room
    })
};
const checkStatusUser = (roomId) => {
    const currentRoom = findRoom(roomId);
    if(currentRoom.users.length === 1){
      return
    }
    return !currentRoom.users.find(user => user.status === false)
};
const changeStatusGame = (roomId, status, idText) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.status = status;
            room.target = texts[idText].length
        }
        return room;
    })
};
const getPercentageProgress = (roomId, username) => {
    return Math.floor((findUserInfoInRoom(roomId, username).progress / findRoom(roomId).target) * 100)
};
const checkProgressUser = (roomId, username) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.users = room.users.map(user => {
                if (user.name === username && user.progress === room.target) {
                    user.finished = new Date();
                }
                return user
            })
        }
        return room
    })
};
const setTimeFinish = (roomId, timeFinished) => {
    rooms = rooms.map(room => {
        if (room.roomId === roomId) {
            room.users = room.users.map(user => {
                if (!user.finished) {
                    user.finished = timeFinished;
                }
                return user
            })
        }
        return room
    })
};
const getListRating = (roomId) => {
    const room = findRoom(roomId);
    return room?.users.sort((a, b) => {
        if ((a.finished - a.progress) > (b.finished - b.progress)) {
            return 1
        }
        if ((a.finished - a.progress) < (b.finished - b.progress)) {
            return -1
        }
        if ((a.finished - a.progress) === (b.finished - b.progress)) {
            return 0
        }
    })
};

const leaveRoom = (io, socket, username) => {
    const currentId = getCurrentRoomId(socket);
    if (!currentId) {
        return
    }

    socket.leave(currentId, () => {
        const currentRoom = findRoom(currentId);
        if (currentRoom.users.length === 1) {
            deleteRoom(currentId);
        } else {
            deleteUserInRoom(currentId, username);
            io.to(currentId).emit("UPDATE_USERS_IN_ROOM", findRoom(currentId));
            if (checkStatusUser(currentId) && !currentRoom.status) {
                startLoadingGame(io, currentId)
            }

          if(currentRoom.status && checkFinishGame(currentId)){
            finishGame(currentId)
          }
        }

        io.emit("UPDATE_ROOMS", availableRoom(rooms));
    });
};
const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId => rooms.find(room => room.roomId === roomId));
const randomValue = () => {
    return Math.floor(Math.random() * 6)
};
const availableRoom = (rooms) => rooms.filter(room => (room.users?.length < config.MAXIMUM_USERS_FOR_ONE_ROOM && !room.status));
const startGame = (io, roomId, idText) => {
    let secondsForGame = config.SECONDS_FOR_GAME;

    io.to(roomId).emit("START_GAME", secondsForGame);
    changeStatusGame(roomId, true, idText);

    let timerGameId = setInterval(() => {
      if(checkFinishGame(roomId)){
        clearInterval(timerGameId);
        return
      }
        io.to(roomId).emit("CHANGE_TIMER_FOR_GAME", --secondsForGame);
    }, 1000);

    setTimeout(() => {
        clearInterval(timerGameId);
        finishGame(roomId);
        io.to(roomId).emit("FINISH_GAME", getListRating(roomId));
    }, secondsForGame * 1000);
};
const checkFinishGame = (roomId) => {
  const currentRoom = findRoom(roomId);
  return !currentRoom?.users.find(user => user.finished === undefined)
};
const finishGame = (roomId) => {
    const timeFinished = new Date();
    setTimeFinish(roomId, timeFinished);
};
const startLoadingGame = (io, roomId) => {
    let secondBeforeStartGame = config.SECONDS_TIMER_BEFORE_START_GAME;

    const idText = randomValue();
    io.to(roomId).emit("START_TIMER", {idText, secondBeforeStartGame});

    let timerId = setInterval(() => {
        io.to(roomId).emit("CHANGE_TIMER", --secondBeforeStartGame);
    }, 1000);

    setTimeout(() => {
        clearInterval(timerId);
        startGame(io, roomId, idText);
    }, config.SECONDS_TIMER_BEFORE_START_GAME * 1000);
};

export default io => {
    io.on("connection", socket => {
        const username = socket.handshake.query.username;

        if (checkUser(username)) {
            socket.emit("ERROR_LOGIN", 'this login already exists');
            return
        }
        addUserList(username);

        socket.emit("UPDATE_ROOMS", availableRoom(rooms));

        socket.on("CREATE_ROOM", roomId => {
            if (!roomId) {
                return;
            }
            const checkRoom = rooms.hasOwnProperty(roomId);
            if (checkRoom) {
                socket.emit("ERROR", "Комната с таким названием уже существует");
                return;
            }

            const users = [createUser(username)];
            rooms.push({roomId, users, status: false, target: 0});

            socket.join(roomId, () => {
                const roomInfo = findRoom(roomId);
                socket.emit("JOIN_ROOM_DONE", roomInfo);
            });

            io.emit("UPDATE_ROOMS", availableRoom(rooms));
        });

        socket.on("JOIN_ROOM", roomId => {
            const prevRoomId = getCurrentRoomId(socket);
            if (roomId === prevRoomId) {
                return;
            }
            if (prevRoomId) {
                socket.leave(prevRoomId);
            }
            const room = findRoom(roomId);
            if(room.status){
              return;
            }
            joinUserInRoom(roomId, username);

            socket.join(roomId, () => {
                io.to(socket.id).emit("JOIN_ROOM_DONE", findRoom(roomId));
            });
            io.to(roomId).emit("UPDATE_USERS_IN_ROOM", findRoom(roomId));
            io.emit("UPDATE_ROOMS", availableRoom(rooms));
        });

        socket.on("LEAVE_ROOM", () => {
            leaveRoom(io, socket, username);
            socket.emit("LEAVE_ROOM_DONE");
            socket.emit("UPDATE_ROOMS", availableRoom(rooms));
        });

        socket.on("CHANGE_USER_STATUS", () => {
            const roomId = getCurrentRoomId(socket);
            if (!roomId) {
                return
            }

            changeUserStatus(roomId, username);
            io.to(roomId).emit("UPDATE_USERS_IN_ROOM", findRoom(roomId));
            socket.emit("CHANGE_USER_STATUS", findStatusUser(roomId, username));

            if (checkStatusUser(roomId)) {
                startLoadingGame(io, roomId)
            }
        });

        socket.on("CHANGE_PROGRESS", () => {
            const roomId = getCurrentRoomId(socket);
            changeUserProgress(roomId, username);
            io.to(roomId).emit("CHANGE_PROGRESS_USER", {
                userId: username,
                progress: getPercentageProgress(roomId, username)
            });
            checkProgressUser(roomId, username);
            if(checkFinishGame(roomId)){
              finishGame(roomId);
              io.to(roomId).emit("FINISH_GAME", getListRating(roomId));
            }
        });

        socket.on("disconnecting", () => {
            leaveRoom(io, socket, username);
            removeUserList(username);
            socket.emit("UPDATE_ROOMS", availableRoom(rooms));
        });

    });
};
